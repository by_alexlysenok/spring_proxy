package by.issoft.training.springproxing.service;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service
@Scope(proxyMode = ScopedProxyMode.DEFAULT)
public class TestServiceImpl implements TestService {
    private final Logger logger = Logger.getLogger("TestServiceImpl");

    public void testMethod1() {
        this.logger.info("log4j: testMethod1 executed");
        System.out.println("TestServiceImpl: testMethod1 executed");
    }
}
