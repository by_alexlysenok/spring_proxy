package by.issoft.training.springproxing;

import by.issoft.training.springproxing.configuration.AppConfig;
import by.issoft.training.springproxing.service.TestService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(final String[] args)
    {
        final ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        final TestService testService = context.getBean(TestService.class);
        testService.testMethod1();
    }
}