package by.issoft.training.springproxing.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogAspect {

    @Before("target(org.apache.log4j.Logger)")
    public void beforeLogAdvice2(){
        System.out.println("ASPECT FOR LOG4J");
    }
}
